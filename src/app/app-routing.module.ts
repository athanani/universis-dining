import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './layouts/index.component';
import { AuthGuard } from '@universis/common';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'dining',
    pathMatch: 'full',
  },
  {
    path: '',
    component: IndexComponent,
    canActivateChild: [
      AuthGuard
    ],
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'dining',
        loadChildren: () => import('@universis/ngx-dining/admin').then( m => m.AdminDiningModule ) 
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes
    // for debugging purposes only use :
    // , { enableTracing: true }
  )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
