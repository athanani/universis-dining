// tslint:disable: trailing-comma
export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'DiningRequests',
    key: 'Sidebar.DiningRequests',
    url: '/dining/requests',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'DiningCards',
    key: 'Sidebar.DiningCards',
    url: '/dining/cards',
    icon: 'fa fa-id-card',
    index: 10
  },
  {
    name: 'Messages',
    key: 'Sidebar.Messages',
    url: '/dining/messages',
    icon: 'fa fa-envelope',
    index: 20
  },
  {
    name: 'DiningPreference',
    key: 'Sidebar.DiningPreference',
    url: '/dining/dining-preference',
    icon: 'fa fa-utensils',
    index: 20
  }
];
